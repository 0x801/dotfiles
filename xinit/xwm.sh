#!/bin/bash

desktops=( 'dwm' 'xmonad' )
xinit_dir='/home/justin/git/dotfiles/xinit'

launch_desktop()
{
	xinit_file=$xinit_dir/$found.xinit

	if [[ -f $xinit_file ]]; then
		ln -sf $xinit_file $HOME/.xinitrc
		startx
	else
		echo "'$xinit_file' not found."
	fi
}

#=======
# MAIN
#=======
# List all desktop options
if [[ $1 = 'list' ]]; then
	for d in ${desktops[@]}; do
		echo $d
	done

# Handle given desktop option
else
	# A parameter must be given before parsing through the list of desktops
	if [[ -n $1 ]]
	then
		found=''
		for d in ${desktops[@]}; do
			if [[ $1 = $d ]]; then
				# Match found
				found=$d
				break
			fi
		done
	
		if [[ -z $found ]]; then
			echo "'$1' not recognized."
		else
			launch_desktop
		fi
	else
		echo "Please enter a desktop option."
	fi
fi

