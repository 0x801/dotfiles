import XMonad
import System.Exit
import qualified XMonad.StackSet as W

-- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops

-- X11 libraries
import Graphics.X11.ExtraTypes.XF86

-- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.ThreeColumns
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral

-- Layout modifiers
import XMonad.Layout.Gaps
import XMonad.Layout.Spacing

-- Utilities
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.Run
import XMonad.Util.Ungrab

-- Data
import Data.Monoid
import qualified Data.Map as M

---------------
-- Variables --
---------------
-- Launchers
myTerminal   :: String
myTerminal    = "alacritty"
myRunner     :: String
myRunner      = "rofi -combi-modi window,drun,ssh -show combi -icon-theme \"Papirus\" -show-icons -dpi 1"
myLockScreen :: String
myLockScreen  = "i3lock-fancy -p"
myBrowser    :: String
myBrowser     = "firefox"

-- Bar
myBar :: String
myBar  = "polybar"
myPP :: PP
myPP  = xmobarPP { ppCurrent = xmobarColor "#000000" "" } -- text written onto bar
toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b) -- key binds to toggle bar's gap

-- Window separators
myBorderPx           :: Dimension
myBorderPx            = 2
mySpacingPx          :: Int
mySpacingPx           = 2
myGapPx              :: Int
myGapPx               = 1
myNormalBorderColor  :: String
myNormalBorderColor   = "#f2f2f2"
myFocusedBorderColor :: String
myFocusedBorderColor  = "#634cf8"

-- Misc.
myModMask    :: KeyMask
myModMask     = mod4Mask
myWorkspaces :: [String]
myWorkspaces  = ["1","2","3","4","5","6","7","8","9"]

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse  = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses  = False


------------------
-- Key bindings --
------------------
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- Spawn applications
    [ ((modm              , xK_Return), spawn (myTerminal)  )
    , ((modm              , xK_p     ), spawn (myRunner)    )
    , ((modm .|. shiftMask, xK_l     ), spawn (myLockScreen))
    , ((modm              , xK_f     ), spawn (myBrowser)   )

    -- Modify master area
    , ((modm, xK_m    ), windows W.swapMaster         ) -- swap the focused window and the master window
    , ((modm, xK_equal), sendMessage (IncMasterN   1) ) -- add a window to the master area
    , ((modm, xK_minus), sendMessage (IncMasterN (-1))) -- remove a window from the master area
    , ((modm, xK_h    ), sendMessage Shrink           ) -- shrink the master area
    , ((modm, xK_l    ), sendMessage Expand           ) -- expand the master area

    -- Change focus windows
    , ((modm              , xK_j), windows W.focusDown) -- move focus down the stack
    , ((modm              , xK_k), windows W.focusUp  ) -- move focus up the stack
    , ((modm .|. shiftMask, xK_j), windows W.swapDown ) -- move the focused window down the stack
    , ((modm .|. shiftMask, xK_k), windows W.swapUp   ) -- move the focused window up the stack

    -- Layouts
    , ((modm, xK_Tab  ), sendMessage NextLayout            ) -- cycle through the available layout with layout algorithm
    , ((modm, xK_space), setLayout $ XMonad.layoutHook conf) -- reset the layouts on the current workspace to default

    -- System functions
    , ((0, xF86XK_MonBrightnessUp  ), spawn "brightnessctl set 5%+")
    , ((0, xF86XK_MonBrightnessDown), spawn "brightnessctl set 5%-")

    -- System shortcuts
    , ((modm .|. shiftMask, xK_space), withFocused $ windows . W.sink              ) -- push window back from floating to tiling
    , ((mod1Mask          , xK_F4   ), kill                                        ) -- close focused window
    , ((modm .|. shiftMask, xK_q    ), io (exitWith ExitSuccess)                   ) -- quit xmonad
    , ((modm              , xK_q    ), spawn "xmonad --recompile; xmonad --restart") -- recompile and restart xmonad (maintains session)
    ]
    ++

    -- Workspaces
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]] 
    ++
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


--------------------
-- Mouse bindings --
--------------------
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


-------------
-- Layouts --
-------------
myLayout = tiled
       ||| spirals
       ||| grid
       ||| threeCol
       ||| threeRow
       ||| monogap
    where
        -- Layouts
        grid     = gapmod
                 $ spacemod
                 $ Grid (16/10)
        monocle  = Full
        monogap  = gapmod
                 $ Full
        spirals  = spacemod
                 $ gapmod
                 $ spiral (6/7)
        threeCol = spacemod
                 $ gapmod
                 $ ThreeCol nm dt rt
        threeRow = gapmod
                 $ spacemod
                 $ Mirror
                 $ threeCol
        tiled    = spacemod
                 $ gapmod
                 $ Tall nm dt rt
        -- Config data
        nm = 1     -- The default number of windows in the master pane
        dt = 3/100 -- Percent of screen to increment by when resizing panes
        rt = 1/2   -- Default proportion of screen occupied by master pane
        -- Gaps
        spacemod = spacing mySpacingPx
        gapmod   = gaps [(U,myGapPx), (D,myGapPx), (L,myGapPx), (R,myGapPx)]


------------------
-- Window rules --
------------------

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = return ()

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = return ()


main :: IO ()
main = xmonad
     . ewmh
--   =<< xmobar myConfig
   =<< statusBar myBar myPP toggleStrutsKey myConfig
myConfig = def {
    -- base
    terminal           = myTerminal,
    focusFollowsMouse  = myFocusFollowsMouse,
    clickJustFocuses   = myClickJustFocuses,
    borderWidth        = myBorderPx,
    modMask            = myModMask,
    workspaces         = myWorkspaces,
    normalBorderColor  = myNormalBorderColor,
    focusedBorderColor = myFocusedBorderColor,
    -- key bindings
    keys               = myKeys,
    mouseBindings      = myMouseBindings,
    -- hooks, layouts
    layoutHook         = myLayout,
    manageHook         = myManageHook,
    handleEventHook    = myEventHook,
    logHook            = myLogHook,
    startupHook        = myStartupHook
    }

